# Tutorial WebRTC - Comunicação P2P entre navegadores

O objetivo deste tutorial é dar um overview sobre a tecnologia WebRTC, em aspectos sobre como funciona, vantagens e desvantagens de se utilizar, arquitetura, etc.

Também vamos demonstrar através de uma aplicação real, como utilizar a tecnologia. 

## Link para vídeo sobre tutorial no Youtube 

Para acessar o vídeo explicativo sobre o tutorial, basta entrar em: [https://youtu.be/m0zpePyKjDs](https://youtu.be/m0zpePyKjDs)

## Sobre WebRTC

A grande promessa do WebRTC é a de que você pode utilizar videochamadas pelo navegador sem utilizar nenhum plugin. _**É tipo "Skype" para navegador.**_

Antes do WebRTC, até era possível criar ferramentas de videochamadas em aplicações desktop ou aplicativos nativos para celulares, porém era necessário um **vasto conhecimento sobre telecomunicações** para atingir o resultado esperado. Não era possível fazer isso facilmente pelo navegador. 

Um dos serviços web de videochamadas mais populares atualmente e que usa WebRTC  [Appear.in](https://www.appear.in). 

## Como funciona

![WebRTC Arquitetura](https://p40.p3.n0.cdn.getcloudapp.com/items/rRuvRRpN/webrtc_basico_diagrama.png) _Diagrama de funcionamento WebRTC. Fonte: WebRTC Ventures_

Dois usuários fazem um _handshake_ inicial através de um _signalling server_ (servidor de sinalização, fundo da imagem).

Esse servidor vai fazer a configuração do RTCPeerConnection, que vai criar uma conexão criptografada P2P entre os dois usuários. Assim que a conexão for criada, eles conseguirão compartilhar vídeo, áudio e dados entre si.

Antes do WebRTC, caso quisesse fazer uma videochamada utilizando o navegador, era necessário a utilização de um servidor para fazer a transmissão dos dados de um cliente para o outro, através de um processo muito mais complexo, o que dificultava a **escalabilidade** de aplicações do tipo, além de segurança gerada pela conexão criptografada entre os clientes.

## Arquitetura
![WebRTC Arquitetura](https://p40.p3.n0.cdn.getcloudapp.com/items/E0u8v0L5/arq.png) _Overview da arquitetura do WebRTC. Fonte: https://webrtc.org/architecture/_

Vale destacar dois pontos na arquitetura: 

1. Desenvolvedores de navegadores estarão interessados na API WebRTC C++ e nas ferramentas de captura e renderização; 

2. Desenvolvedores web estarão interessados na [API Web](http://w3c.github.io/webrtc-pc/). 


### Signaling 

Antigamente, quando se queria fazer uma ligação de longa distância, era necessário um "operador telefônico humano" para completar a ligação. Ao utilizar WebRTC existe um processo similar, não feito por humano, mas por um servidor, que vai fazer o _handshake_ entre os dois usuários. 

Essa é a parte da tecnologia que **não utiliza conexão P2P**. Esse "aperto de mão" é feito utilizando a arquitetura Cliente / Servidor.

Existem várias formas de implementar o processo de _signaling_. Neste tutorial, vamos utilizar Socket.io. Mas é possível utilizar outras metodologias, inclusive pub/sub. 

O conceito básico ao se utilizar signaling é o conceito de oferta / resposta. 

![Diagrama Signaling](https://p40.p3.n0.cdn.getcloudapp.com/items/lluZlGyy/signaling.png) _Diagrama - Signaling. Fonte: WebRTC Ventures_

## Sobre as conexões WebRTC: ICE, STUN e TURN

Na tentativa de criar uma conexão P2P entre dois clientes, surge a dúvida: como lidar com o fato de que a maioria dos endpoints podem estar com IPs privados e protegidos por Firewall? 

Para entender esse conceito, é ideal definir o que é IP Público e IP Privado: 

1. **Public IP Address (Endereço de IP Público):** é o endereço de IP único que cada máquina que está conectada na internet possui. 

2. **Private IP Address (Endereço de IP Privado):** ao contrário do IP público, ele não é único e pode existir simultâneamente em diversas máquinas ao mesmo tempo. 

Nesse contexto, entra em atuação servidores STUN / TURN, conforme mostrado na ilustração abaixo: 

![Stun / Turn servers](https://p40.p3.n0.cdn.getcloudapp.com/items/NQubG6ej/stun.png) _Diagrama - Stun / Turn servers _

O STUN (Simple Traversal of User Datagram Protocol [UDP], por meio da Network Address Translators [NATs]), é um servidor que permite que clientes NAT (ex.: computadores protegidos por firewall) descubram seu endereço público, o tipo de NAT utilizado, e o lado da porta da Internet associada à NAT com uma porta local específica.

Entretanto, dependendo da rede, pode existir restrições ainda mais complexas (Ex: proibir tráfego UDP, apenas TCP permitido, etc...). Quando isso acontece, é necessário a utilização de TURN (Traversal Using Relays around NAT) Servers. 

De qualquer forma, o protocolo ICE (Interactive Connectivity Establishment) é usado para encontrar a melhor solução. 

## Principais APIs usadas pelo WebRTC

### GetUserMedia

Permite o acesso a câmera, microfone ou a tela do dispositivo.

### RTCPeerConnection

Faz o encode / decode da mídia, envia para a rede, encapsula NAT transversal, etc... 

### RTCDataChannel

Representa um canal de comunicação bi-direcional entre os peers da conexão. 


## Prós e contras de usar WebRTC

### Prós

1. Não é necessário utilizar nenhum plugin adicional
2. Comunicação P2P (sem intermediários)
3. Pouca utilização de tráfego nos servidores
4. Baixa latência
5. Criptografia na conexão

### Contras

1. Não é suportado por todos os navegadores
2. Não é suportado por nenhum browser em iOS
3. Não funciona para muitas conexões ao mesmo tempo (ideal entre 4 e 8 participantes)


## Quem utiliza WebRTC?

1. [Google Meet e Google Hangouts](http://meet.google.com)
2. [Facebook Messenger](https://www.messenger.com/)
3. [Discord](https://discordapp.com/)
4. [Amazon Chime](https://aws.amazon.com/pt/chime/)
5. [Appear.in](https://appear.in)
6. [Gotomeeting](https://www.gotomeeting.com/pt-br)


## Exemplo prático

Para fixar os conceitos sobre o que é o WebRTC, foi desenvolvida uma aplicação simples de videochamada que implementa um servidor de signaling e permite que dois clientes possam criar uma videochamada pelo navegador. 

Na aplicação também está implementado um exemplo simples de chat utilizando a biblioteca Express.io

Stack de tecnologia utilizado no exemplo é: 

1. Linguagem Javascript
2. Nodejs (nvm v0.12.0)
3. Express.io
4. Servidor de aplicação: Heroku

[Clique aqui e veja o teste no servidor do Heroku](https://pacific-depths-50820.herokuapp.com/)

# Referências
* Curso Online Udemy: https://www.udemy.com/introduction-to-webrtc/
* WebRTC.org: https://webrtc.org/
* WebRTC Tutorial - How does WebRTC work?: https://www.youtube.com/watch?v=2Z2PDsqgJP8&t=542s
* WebRTC Basics: https://www.youtube.com/watch?v=-Uons2hxonI&t=677s
* Arin Sime, WebRTC.ventures (Presentation): https://www.slideshare.net/arinsime/webrtc-overview
* Wikipédia: https://en.wikipedia.org/wiki/WebRTC
* WebRTC + Socket.io: building a skype-like video chat with native javascript: https://www.slideshare.net/micheledisalvatore/webrt-socketio-building-a-skypelike-video-chat-with-native-javascript 
* 10 Massive Applications Using WebRTC: https://bloggeek.me/massive-applications-using-webrtc/ 
